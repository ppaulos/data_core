from setuptools import setup

setup(
   name='data_core',
   version='1.0',
   description='Data core module',
   author='Philippe Paulos',
   author_email='philippe.paulos@gmail.com',
   packages=['data_core'],
   install_requires=['pyspark'],
)
