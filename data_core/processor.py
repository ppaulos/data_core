import logging
import time
from abc import abstractmethod, ABC
from functools import wraps

from pyspark.sql import SparkSession


def execution_time(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        logging.info(f'Function {func.__name__}{args} {kwargs} Took {total_time:.4f} seconds')
        return result
    return timeit_wrapper


class Processor(ABC):

    def __init__(self, app_name=__file__):
        self.spark = SparkSession.builder.master("local[*]") \
            .appName(app_name) \
            .getOrCreate()

    @execution_time
    @abstractmethod
    def process(self):
        pass
