from pyspark.sql import SparkSession, DataFrame


class FileType:
    CSV = 'csv'
    PARQUET = 'parquet'


def load_df(spark: SparkSession, path: str, options: dict, file_type: FileType) -> DataFrame:
    df_reader = spark.read.options(**options)
    if file_type == 'csv':
        return df_reader.csv(path)
    elif file_type == FileType.PARQUET:
        return df_reader.parquet(path)
    else:
        raise NotImplemented(f"{file_type} is not implemented")
