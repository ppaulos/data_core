import logging

from data_core.processor import Processor


class Logger:
    def __init__(self):
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        logger_handler = logging.StreamHandler()  # Handler for the logger
        logger.addHandler(logger_handler)
        formatter = logging.Formatter('[%(asctime)s] %(filename)s:%(lineno)d %(levelname)s - %(message)s','%m-%d %H:%M:%S')
        logger_handler.setFormatter(formatter)


def run(processor: Processor):
    Logger()
    processor.process()
